
function onComputeFloor()
{
    let floorInput = document.querySelector('#problemInputArea').value

    if(floorInput.length == 0)
    {
        alert("No floor input provided.")
    }
    else
    {
        let metrics = computeFloorMetrics(floorInput)

        if(metrics)
        {
            document.querySelector('#finalFloorLbl').textContent = metrics['finalFloor']
            document.querySelector('#firstBasementIdxLbl').textContent = metrics['firstBasementIdx']
        }
        else
        {
            document.querySelector('#finalFloorLbl').textContent = "Error in input."
        }


    }   
}

function computeFloorMetrics(floorInput)
{
    let floor = 0
    let firstBasementIdx = 0

    for (let i = 0; i < floorInput.length; ++i)
    {
        if(floorInput.charAt(i) == '(')
        {
            floor++
        }
        else if(floorInput.charAt(i) == ')')
        {
            floor--
        }
        else
        {
            alert("Unrecognized character '" + floorInput.charAt(i) + "' in input.");
        }

        if(firstBasementIdx == 0 && floor == -1)
        {
            firstBasementIdx = i + 1
        }
    }

    return {
        'finalFloor': floor,
        'firstBasementIdx': firstBasementIdx
      }
}

export {onComputeFloor}
