
// Used to hash 2D integer coordinates into a single integer
const HASHING_PRIME = 1217
// Stores [2d coordinate hash, number of presents]
let giftsPerHouse = new Map()

export function solve() {
    const rawData = document.querySelector('#problemInputArea').value

    if (rawData.length == 0) {
        alert("No data provided.")
    }
    else {
        executeDeliveries(rawData, false)
        const atLeast1SoloSanta = computeAtLeast1Delivery()

        giftsPerHouse.clear()
        executeDeliveries(rawData, true)
        const atLeast1RoboSanta = computeAtLeast1Delivery()

        if (atLeast1SoloSanta) {
            document.querySelector('#part1Lbl').textContent = atLeast1SoloSanta
            document.querySelector('#part2Lbl').textContent = atLeast1RoboSanta
        }
        else {
            document.querySelector('#part1Lbl').textContent = "Error in input."
        }

    }
}

function executeDeliveries(rawData, enableRoboSanta) {

    let santaCoord = {row: 0, col: 0}
    let roboCoord = {row: 0, col: 0}
    let santaTurn = true

    deliverPresentAt(santaCoord.row, santaCoord.col)

    if(enableRoboSanta)
    {
        // Santa and robosanta both make a delivery at the start
        deliverPresentAt(roboCoord.row, roboCoord.col)
    }

    for (let direction of rawData) {
        if(enableRoboSanta && !santaTurn)
        {
            navigate(roboCoord, direction)
            deliverPresentAt(roboCoord.row, roboCoord.col)
            santaTurn = true
        }
        else
        {
            navigate(santaCoord, direction)
            deliverPresentAt(santaCoord.row, santaCoord.col)
            santaTurn = false
        }
    }
}

function navigate(indecies, direction)
{
        // Navigate
        switch (direction) {
            case '^':
                indecies.row++
                break;
            case 'v':
                indecies.row--
                break;
            case '<':
                indecies.col--
                break;
            case '>':
                indecies.col++
                break;
            default:
                alert('Unknown symbol in input data: ' + direction)
        }
}

function deliverPresentAt(rowIdx, colIdx) {
    const hashVal = hash2DCoordinate(rowIdx, colIdx)
    if(giftsPerHouse.has(hashVal))
    {
        let numGiftsAtHouse = giftsPerHouse.get(hashVal)
        giftsPerHouse.set(hashVal, numGiftsAtHouse + 1)
    }
    else
    {
        giftsPerHouse.set(hashVal, 1)
    }
}

function computeAtLeast1Delivery()
{
    //console.log(giftsPerHouse)

    let atLeast1 = 0
    for (const entry of giftsPerHouse.entries())
    {
        atLeast1++
    }
    return atLeast1
}

function hash2DCoordinate(rowIdx, colIdx)
{
    //const hashVal = HASHING_PRIME * rowIdx + colIdx
    //console.log(rowIdx + "," + colIdx + ":" + hashVal)
    return HASHING_PRIME * rowIdx + colIdx
}