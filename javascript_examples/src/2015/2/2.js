
function solve()
{
    const rawSizes = document.querySelector('#problemInputArea').value

    if(rawSizes.length == 0)
    {
        alert("No size data provided.")
    }
    else
    {
        const dimensions = parseSizes(rawSizes)
        const totalSqFt = computeSqFtPerSide(dimensions)
        const totalRibbonFt = computeRibbonLength(dimensions)

        if(totalSqFt)
        {
            document.querySelector('#totalSqFtLbl').textContent = totalSqFt
            document.querySelector('#totalRibbonFtLbl').textContent = totalRibbonFt
        }
        else
        {
            document.querySelector('#totalSqFtLbl').textContent = "Error in input."
        }

    }   
}

function parseSizes(rawSizesStr)
{
    // Split the input string into an array of LxWxH strings.
    const sizesAsStrArr = rawSizesStr.split('\n')

    // Check if the last element is a newline token and remove it.
    if(sizesAsStrArr.at(-1).length == 0)
    {
        sizesAsStrArr.pop()
    }

    // Map each raw size string to square feet per item
    const dimensionsPerItem = sizesAsStrArr.map((lwhStr) => {
        // Tokenize the LxWxH string into strings of numbers
        let dimensionsStr = lwhStr.split('x')
        const itemLength = parseInt(dimensionsStr[0])
        const itemWidth = parseInt(dimensionsStr[1])
        const itemHeight = parseInt(dimensionsStr[2])

        return [itemLength, itemWidth, itemHeight]
    });
    return dimensionsPerItem
}


function computeSqFtPerSide(allDimensions)
{
    // Map each each set of dimensions to total square ft per item
    const sqFtPerItem = allDimensions.map((itemDimensions) => {
        const itemLength = itemDimensions[0]
        const itemWidth = itemDimensions[1]
        const itemHeight = itemDimensions[2]

        // Compute square feet per side
        const sqftPerDimension = [
            // 2*l*w
            2 * itemLength * itemWidth,
            // 2*w*h
            2 * itemWidth * itemHeight,
            // 2*h*l 
            2 * itemHeight * itemLength
        ]

        // ... auto-expands/unpacks the sqftPerDimension array
        // Divide by 2 since each sqftPerDimension accounts for two
        // sides of the box. We only need enough slack paper for a
        // single side.
        const minSide = Math.min(...sqftPerDimension) / 2

        // Sum the sqftPerDimension array
        const sideSummation = sqftPerDimension.reduce(
            (accumulator, currentValue) => accumulator + currentValue);

        // Combine size of item plus enough slack to cover the minimum side again.
        return sideSummation + minSide
    });

    // Sum up square feet of all items.
    return sqFtPerItem.reduce(
        (accumulator, currentValue) => accumulator + currentValue);
}


function computeRibbonLength(allDimensions)
{
    // Map each each set of dimensions to total ribbon length
    const ribbonPerItem = allDimensions.map((itemDimensions) => {
        const itemLength = itemDimensions[0]
        const itemWidth = itemDimensions[1]
        const itemHeight = itemDimensions[2]

        const bowLength = itemLength * itemWidth * itemHeight

        // ... auto-expands/unpacks the array
        let eraseIdx = itemDimensions.indexOf(Math.max(...itemDimensions));
        // "Add in" one element at index eraseIdx...but don't provide the element.
        // This causes the element at eraseIdx to be deleted without leaving a
        // null/undefined hole in the array.
        itemDimensions.splice(eraseIdx, 1)

        // Since the max dimension was removed, we're guaranteed to compute the
        // perimeter around the shortest face of the box.
        const ribbonWrapLength = itemDimensions[0] * 2 + itemDimensions[1] * 2

        // Total ribbon length for the box
        return bowLength + ribbonWrapLength
    });

    return ribbonPerItem.reduce(
        (accumulator, currentValue) => accumulator + currentValue);
}

export {solve}
