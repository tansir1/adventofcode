import networkx as nx

location_graph = nx.Graph()
loc_to_idx = {}
idx_to_loc = {}
location_cnt = 0


def parse_line(raw_line):
    global location_cnt
    global location_graph
    global loc_to_idx
    global idx_to_loc

    tokens = raw_line.split(' ')
    start_token = tokens[0]
    end_token = tokens[2]
    travel_cost = int(tokens[4])

    if start_token not in loc_to_idx:
        loc_to_idx[start_token] = location_cnt
        idx_to_loc[location_cnt] = start_token
        location_graph.add_node(location_cnt)
        location_cnt += 1

    if end_token not in loc_to_idx:
        loc_to_idx[end_token] = location_cnt
        idx_to_loc[location_cnt] = end_token
        location_graph.add_node(location_cnt)
        location_cnt += 1

    start_idx = loc_to_idx[start_token]
    end_idx = loc_to_idx[end_token]
    location_graph.add_edge(start_idx, end_idx)
    location_graph[start_idx][end_idx]['weight'] = travel_cost


def compute_from_file(path):
    with open(path) as input_file:
        row_cnt = 0
        raw_line = input_file.readline()
        raw_line = raw_line.strip()
        while len(raw_line) > 0:
            row_cnt += 1
            print(f'Parsing input row {row_cnt}.')
            parse_line(raw_line)
            raw_line = input_file.readline()
            raw_line = raw_line.strip()


def test_logic():
    parse_line('London to Dublin = 464')
    parse_line('London to Belfast = 518')
    parse_line('Dublin to Belfast = 141')


def compute_path_cost(tsp_path):
    global idx_to_loc

    print('Path:')

    total_cost = 0
    for i in range(1, len(tsp_path)):
        edge_cost = location_graph[tsp_path[i-1]][tsp_path[i]]['weight']
        start_loc = idx_to_loc[tsp_path[i-1]]
        end_loc = idx_to_loc[tsp_path[i]]
        print(f'{start_loc} -> {end_loc} = {edge_cost}')
        total_cost += edge_cost
    print(total_cost)


if __name__ == '__main__':
    #test_logic()
    compute_from_file('./input_9.txt')
    result = nx.approximation.traveling_salesman_problem(location_graph, cycle=False)
    compute_path_cost(result)


