import networkx as nx

# Could solve with a graph or tree model...or we could just let a massive recursive call stack unwind it all.

wire_ops = {}
cache_results = {}


def recursive_eval(raw_input):
    # termination condition when we get a raw number
    try:
        return int(raw_input)
    except ValueError:
        pass

    if raw_input not in cache_results:
        gate_ops = wire_ops[raw_input]
        if type(gate_ops) is str:
            # Single element, so it should be terminal signal input
            # and will pass the 'int' cast above.
            output = recursive_eval(gate_ops)
        else:  # gate_ops is a list of 3 elements.
            if len(gate_ops) == 3:
                gate_type = gate_ops[1]
            else:  # 'Not' case
                gate_type = gate_ops[0]
            match gate_type:
                case 'AND':
                    output = recursive_eval(gate_ops[0]) & recursive_eval(gate_ops[2])
                case 'RSHIFT':
                    output = recursive_eval(gate_ops[0]) >> recursive_eval(gate_ops[2])
                case 'LSHIFT':
                    output = recursive_eval(gate_ops[0]) << recursive_eval(gate_ops[2])
                case 'OR':
                    output = recursive_eval(gate_ops[0]) | recursive_eval(gate_ops[2])
                case 'NOT':
                    output = ~recursive_eval(gate_ops[1]) & 0xffff
                case _:
                    print(f'Error: UNKNOWN OPERATOR! {gate_type}')
        cache_results[raw_input] = output

    return cache_results[raw_input]


def parse_line(raw_line):
    signals, result = raw_line.split('->')
    signals = signals.strip()
    result = result.strip()

    multi_sig_check = signals.split(' ')
    if len(multi_sig_check) > 1:
        signals = multi_sig_check
    wire_ops[result] = signals


def compute_from_file(path):
    with open(path) as input_file:
        row_cnt = 0
        raw_line = input_file.readline()
        raw_line = raw_line.strip()
        while len(raw_line) > 0:
            row_cnt += 1
            #print(f'Parsing input row {row_cnt}.')
            parse_line(raw_line)
            raw_line = input_file.readline()
            raw_line = raw_line.strip()


def test_logic():
    parse_line('123 -> x')
    parse_line('456 -> y')
    parse_line('x AND y -> d')
    parse_line('x OR y -> e')
    parse_line('x LSHIFT 2 -> f')
    parse_line('y RSHIFT 2 -> g')
    parse_line('NOT x -> h')
    parse_line('NOT y -> i')
    print(recursive_eval('f'))


if __name__ == '__main__':
    # test_logic()

    compute_from_file('./input_7.txt')
    part1_result = recursive_eval('a')
    print('a={}'.format(part1_result))

    # Part 2 override
    wire_ops.clear()
    cache_results.clear()
    compute_from_file('./input_7.txt')
    cache_results['b'] = part1_result
    print('a={}'.format(recursive_eval('a')))


