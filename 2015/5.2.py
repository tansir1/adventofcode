
# https://adventofcode.com/2015/day/5


def contains_any_pair(test_data: str):
    for i in range(len(test_data) - 1):
        pair: str = test_data[i:i+2]
        if -1 != test_data.find(pair, i + 2):
            return True
    return False


def repeat_skip_one_letter(test_data: str):
    for i in range(len(test_data) - 2):
        if test_data[i] == test_data[i+2]:
            return True
    return False


def is_nice(test_data):
    contain_pair: bool = contains_any_pair(test_data)
    repeat_skip: bool = repeat_skip_one_letter(test_data)

    nice_result = contain_pair and repeat_skip
    if nice_result:
        print(f'{test_data} is nice.')
    else:
        print(f'{test_data} is naughty.')
    return nice_result


def check_file(path):
    num_nice: int = 0
    with open(path) as input_file:
        raw_line = input_file.readline()
        raw_line = raw_line.strip()

        while len(raw_line) > 0:
            if is_nice(raw_line):
                num_nice += 1
            raw_line = input_file.readline()
            raw_line = raw_line.strip()
    print(f'Num nice: {num_nice}')


def test_sample_data():
    assert is_nice('qjhvhtzxzqqjkmpb')
    assert is_nice('xxyxx')
    assert not is_nice('uurcxstgmygtbstg')
    assert not is_nice('ieodomkazucvgmuy')


if __name__ == '__main__':
    test_sample_data()
    check_file('./input_5.txt')



