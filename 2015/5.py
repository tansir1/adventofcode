
# https://adventofcode.com/2015/day/5

def contains3vowels(test_data: str):
    vowel_cnt: int = 0
    vowel_cnt += test_data.count('a')
    vowel_cnt += test_data.count('e')
    vowel_cnt += test_data.count('i')
    vowel_cnt += test_data.count('o')
    vowel_cnt += test_data.count('u')
    return vowel_cnt >= 3


def not_contain_bad_combos(test_data: str):
    # ab, cd, pq, or xy
    bad_cnt: int = 0
    bad_cnt += test_data.count('ab')
    bad_cnt += test_data.count('cd')
    bad_cnt += test_data.count('pq')
    bad_cnt += test_data.count('xy')
    return bad_cnt == 0


def twice_in_a_row(test_data: str):
    for i in range(len(test_data) - 1):
        if test_data[i+1] == test_data[i]:
            return True
    return False


def is_nice(test_data):
    cont3vowels: bool = contains3vowels(test_data)
    bad_combos: bool = not_contain_bad_combos(test_data)
    twice_row: bool = twice_in_a_row(test_data)

    nice_result = cont3vowels and bad_combos and twice_row
    if nice_result:
        print(f'{test_data} is nice.')
    else:
        print(f'{test_data} is naughty.')
    return nice_result


def check_file(path):
    num_nice: int = 0
    with open(path) as input_file:
        raw_line = input_file.readline()
        while len(raw_line) > 0:
            raw_line = raw_line.strip()
            if is_nice(raw_line):
                num_nice += 1
            raw_line = input_file.readline()
    print(f'Num nice: {num_nice}')


def test_sample_data():
    assert is_nice('ugknbfddgicrmopn')
    assert is_nice('aaa')
    assert not is_nice('jchzalrnumimnmhp')
    assert not is_nice('haegwjzuvuyypxyu')
    assert not is_nice('dvszwmarrgswjxmb')


if __name__ == '__main__':
    # test_sample_data()
    check_file('./input_5.txt')



