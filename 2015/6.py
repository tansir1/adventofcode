
# pip install numpy

import numpy as np


#lights = np.zeros((999, 999), dtype=bool)
# +1 in both dimensions to deal with matrix slicing not counting the last element
# and the code adding +1 to the indecies to deal with that.  So any commands on
# the border of the matrix caused us to walk off the array. A +1 padding prevents this.
lights = np.zeros((1000, 1000), dtype=bool)
#lights = np.zeros((10, 10), dtype=bool)
#lights = np.zeros((10, 10), dtype=bool)


def turn_on(top_left_x, top_left_y, bottom_right_x, bottom_right_y):
    global lights

    section_height = max(bottom_right_x - top_left_x + 1, 1)
    section_width = max(bottom_right_y - top_left_y + 1, 1)
    mask = np.ones((section_height, section_width), dtype=bool)
    lights_subregion = lights[top_left_x:bottom_right_x + 1, top_left_y:bottom_right_y + 1]
    lights[top_left_x:bottom_right_x + 1, top_left_y:bottom_right_y + 1] = \
        np.bitwise_or(lights_subregion, mask)


def turn_off(top_left_x, top_left_y, bottom_right_x, bottom_right_y):
    global lights

    section_height = max(bottom_right_x - top_left_x + 1, 1)
    section_width = max(bottom_right_y - top_left_y + 1, 1)
    mask = np.zeros((section_height, section_width), dtype=bool)
    lights_subregion = lights[top_left_x:bottom_right_x + 1, top_left_y:bottom_right_y + 1]
    lights[top_left_x:bottom_right_x + 1, top_left_y:bottom_right_y + 1] = \
        np.bitwise_and(lights_subregion, mask)


def toggle(top_left_x, top_left_y, bottom_right_x, bottom_right_y):
    global lights
    lights[top_left_x:bottom_right_x + 1, top_left_y:bottom_right_y + 1] = \
        np.bitwise_not(lights[top_left_x:bottom_right_x + 1, top_left_y:bottom_right_y + 1])


def parse_xy(raw_str: str):
    tokens = raw_str.split(',')
    return int(tokens[0]), int(tokens[1])


def parse_compute_line(raw_line):
    tokens = raw_line.split(' ')

    token_idx_toggle_offset = 0
    if tokens[0].startswith('toggle'):
        token_idx_toggle_offset = 1

    tlx, tly = parse_xy(tokens[2 - token_idx_toggle_offset])
    brx, bry = parse_xy(tokens[4 - token_idx_toggle_offset])

    if tokens[1].startswith('on'):
        turn_on(tlx, tly, brx, bry)
    elif tokens[1].startswith('off'):
        turn_off(tlx, tly, brx, bry)
    else:
        toggle(tlx, tly, brx, bry)


def compute_from_file(path):
    with open(path) as input_file:
        row_cnt = 0
        raw_line = input_file.readline()
        raw_line = raw_line.strip()
        while len(raw_line) > 0:
            row_cnt += 1
            print(f'Parsing input row {row_cnt}.')
            parse_compute_line(raw_line)
            raw_line = input_file.readline()
            raw_line = raw_line.strip()


def test_logic():
    global lights
    print(lights)
    #parse_compute_line('turn on 1,1 through 2,3')
    parse_compute_line('toggle 1,1 through 3,4')
    parse_compute_line('turn off 1,1 through 1,2')
    print(lights)


def test1():
    left = np.zeros((3, 3), dtype=int)
    #right = np.ones((2, 3), dtype=int)
    right = np.identity(3, dtype=int)
    print(f'left:\n{left}')
    print(f'right:\n{right}')

    op_test = np.bitwise_or(left, right)
    print(f'op_test:\n{op_test}')


def test2():
    left = np.zeros((3, 3), dtype=int)
    right = [[1, 0], [1, 0]]
    print(f'left:\n{left}')
    print(f'right:\n{right}')

    op_test = np.bitwise_or(left[:2, :2], right)
    print(f'op_test:\n{op_test}')
    left[:2, :2] = op_test
    print(f'left:\n{left}')


def test3():
    x = range(100)
    x = np.reshape(x, (10, 10))
    print(x)
    y = x[1:2, 1:3]
    print(y)


if __name__ == '__main__':
    #test_logic()
    #compute_from_file('./input_6.txt')
    #test2()
    #test3()

    compute_from_file('./input_6.txt')
    num_lit = np.count_nonzero(lights)
    print(f'Num lit: {num_lit}')

