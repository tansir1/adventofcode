
# https://adventofcode.com/2015/day/4

import hashlib
import multiprocessing
import typing

# Part 1
#f ind_hash_str = '00000'
# Part 2
find_hash_str = '000000'


def init_worker(event, key):
    global stop_event
    global secret_key
    stop_event = event
    secret_key = key
    print(f'init_worker: {stop_event}, {secret_key}', flush=True)


def parallel_task(test_rng: typing.List[int]):
    for i in range(test_rng[0], test_rng[1]):
        trial_input = secret_key + str(i)
        hash_result = hashlib.md5(trial_input.encode())
        hash_digest = hash_result.hexdigest()

        if hash_digest.startswith(find_hash_str):
            print(f'Success with input {i} for digest {hash_digest}.')
            return


def run_parallel(key: str):
    test_nums = []

    # Part 1 bounds
    # start = 0
    # end = int(1e7)

    # Part 2 bounds
    start = 0
    end = int(1e10)

    num_cores = multiprocessing.cpu_count() - 1
    chunk_sz = (end - start) // num_cores
    for i in range(1, num_cores):
        test_nums.append([start + (i-1) * chunk_sz, start + i * chunk_sz])

    with multiprocessing.Manager() as manager:
        shared_event = manager.Event()
        with multiprocessing.Pool(initializer=init_worker, initargs=(shared_event, key, )) as pool:
            result = pool.map_async(parallel_task, test_nums)
            result.wait()


if __name__ == '__main__':
    #run_parallel('abcdef')
    run_parallel('iwrupvqb')

