

code_char_cnt = 0
str_char_cnt = 0


def parse_line(raw_line):
    code_len = len(raw_line)

    # Discount the open/close quote marks
    char_len = code_len - 2

    # Check for \x## tokens.
    hex_idx = raw_line.find('\\x')
    while hex_idx > 0:
        # +4 to remove \x##
        raw_line = raw_line.replace(raw_line[hex_idx:hex_idx + 4], '')
        char_len -= 3
        hex_idx = raw_line.find('\\x')
    #print(f'{code_len}, {char_len}')

    # Check for \\ tokens.
    slash_idx = raw_line.find('\\\\')
    while slash_idx > 0:
        # +1 to remove \\
        raw_line = raw_line.replace(raw_line[slash_idx:slash_idx + 1], '')
        char_len -= 1
        slash_idx = raw_line.find('\\\\')
    # print(f'{code_len}, {char_len}')

    # Check for \" tokens.
    quote_idx = raw_line.find('\\"')
    while quote_idx > 0:
        # +1 to remove \"
        raw_line = raw_line.replace(raw_line[quote_idx:quote_idx + 1], '')
        char_len -= 1
        quote_idx = raw_line.find('\\"')
    #print(f'{code_len}, {char_len}')
    return code_len, char_len

def parse_line2(raw_line):
    code_len = len(raw_line)

    # Discount the open/close quote marks
    char_len = code_len - 2

    # Check for \x## tokens.
    hex_idx = raw_line.find('\\x')
    while hex_idx > 0:
        # +4 to remove \x##
        raw_line = raw_line.replace(raw_line[hex_idx:hex_idx + 4], '')
        char_len -= 3
        hex_idx = raw_line.find('\\x')
    #print(f'{code_len}, {char_len}')

    # Check for \\ tokens.
    slash_idx = raw_line.find('\\\\')
    while slash_idx > 0:
        # +1 to remove \\
        raw_line = raw_line.replace(raw_line[slash_idx:slash_idx + 1], '')
        char_len -= 1
        slash_idx = raw_line.find('\\\\')
    # print(f'{code_len}, {char_len}')

    # Check for \" tokens.
    quote_idx = raw_line.find('\\"')
    while quote_idx > 0:
        # +1 to remove \"
        raw_line = raw_line.replace(raw_line[quote_idx:quote_idx + 1], '')
        char_len -= 1
        quote_idx = raw_line.find('\\"')
    #print(f'{code_len}, {char_len}')
    return code_len, char_len

def compute_from_file(path):
    code_len = 0
    char_len = 0
    with open(path) as input_file:
        row_cnt = 0
        raw_line = input_file.readline()
        raw_line = raw_line.strip()
        while len(raw_line) > 0:
            row_cnt += 1
            print(f'Parsing input row {row_cnt}.')
            a, b = parse_line(raw_line)
            code_len += a
            char_len += b

            raw_line = input_file.readline()
            raw_line = raw_line.strip()
    return code_len - char_len


def test_logic():
    #parse_line('""')
    #parse_line('"abc"')
    parse_line('"aaa\"aaa"')
    parse_line('"\x27"')
    print(f'Code: {code_char_cnt}, Str:{str_char_cnt}')


if __name__ == '__main__':
    #test_logic()

    result = compute_from_file('./input_8.txt')
    print(result)
    #part1_result = recursive_eval('a')
    #print('a={}'.format(part1_result))


